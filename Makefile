IMAGE_NAME := langdon/antora-container
SYSTEMD_CONTAINER_NAME := antora-container
DOCKER_FNAME := Dockerfile
SELINUX := :z

help:
		@echo "make build - Build and locally tag a new docker image."
		@echo "make build-force - Use a no-cache build"

build: 
		@docker build --file=$(DOCKER_FNAME) . -t $(IMAGE_NAME)

build-force:
		@docker build --file=$(DOCKER_FNAME) --no-cache . -t $(IMAGE_NAME)



