#!/bin/bash

ID=`stat -c "%u" .`
GROUP=`stat -c "%g" .`

groupadd -g $GROUP antora_grp 
useradd -u $ID -g $GROUP -d /wd -M antora_user
cd /wd

if [ -z $1 ]
then
	echo "" && \
	echo "You can run any antora command (including --help) or " && \
	echo "run-server which will run antora package and then run a webserver " && \
	echo "at http://localhost:8080 and it will update on every change." && \
	echo ""
	exit 0
fi

if [ $1 = 'run-server' ]
then
	echo "not supported yet" 
else
	echo will execute: antora $1
	su antora_user -c "antora $1"
fi

