FROM fedora-minimal
RUN microdnf install gcc openssl-devel libcurl-devel file hostname diffutils less dnf nodejs && \
    microdnf clean all
RUN npm install -g @antora/cli 
 
#RUN npm install -g @antora/site-generator-default
ADD run-it.sh /run-it.sh
RUN mkdir /wd
VOLUME /wd
WORKDIR /wd
EXPOSE 8080
ENTRYPOINT ["/run-it.sh"]


# bash-4.4# history | grep install                                                                                                   
#     1  npm install @antora/site-generator-default                                                                                  
#     3  microdnf install less gcc file hostname diffutils openssl openssl-devel libcurl-devel dnf                                                                   
#     9  microdnf install openssl                                                                                                    
#    10  npm install @antora/site-generator-default                                                                                  
#    15  microdnf install file hostname                                                                                              
#    16  microdnf install diffutils                                                                                                  
#    17  npm install @antora/site-generator-default                                                                                  
#    18  microdnf install libssl-devel                                                                                               
#    19  microdnf install dnf                                                                                                        
#    20  dnf install openssl-devel                                                                                                   
#    21  npm install @antora/site-generator-default                                                                                  
#    22  dnf install libcurl-devel                                                                                                   
#    23  npm install @antora/site-generator-default                                                                                  
#    24  history | grep install   