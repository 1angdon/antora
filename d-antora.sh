#!/bin/bash

docker pull langdon/antora-container
echo docker run -v $PWD:/wd:z --workdir /wd --rm -it langdon/antora-container
docker run -v $PWD:/wd:z --workdir /wd -p 8080:8080 --rm -it langdon/antora-container $1
